import vibe.vibe;
import std.stdio;
import std.file;
import std.process;
import std.datetime.stopwatch: StopWatch, AutoStart;
import core.thread : Thread;

void run(HTTPServerRequest req, HTTPServerResponse res) {
	auto output = File("output.txt", "w");
	auto input = File("input.txt", "w");
	auto source = req.form["source"];
	input.writeln(source);
	input.close();
	auto pid = spawnProcess(["./opp", "../playground-opp/input.txt"], std.stdio.stdin, output, output, null, Config.none, "../opp");
	auto sw = StopWatch(AutoStart.yes);
	auto opp = tryWait(pid);
	while(sw.peek() < 3.seconds && !opp.terminated){
		Thread.sleep(10.msecs);
		opp = tryWait(pid);
	}
	output.close();
	if(opp.terminated){
		res.writeBody(readText("output.txt"));
	} else {
		kill(pid);
		res.writeBody("You have been timed out (your program did not terminate in lesss than 3 seconds)");
	}
}

void index(HTTPServerRequest req, HTTPServerResponse res){
	auto fileName = "file" in req.params ? req.params["file"] : "overview";
	fileName ~= ".opp";
	auto source = readText("public/examples/"~fileName);
	res.render!("index.dt", source, res);
}

void grammar(HTTPServerRequest req, HTTPServerResponse res){
	res.render!("grammar.dt", res);
}

void features(HTTPServerRequest req, HTTPServerResponse res){
	res.render!("features.dt", res);
}

void main(){
	auto router = new URLRouter;
	router.get("/", &index);
	router.post("/run", &run);
	router.get("/example/:file", &index);
	router.get("/grammar", &grammar);
	router.get("/features", &features);
	router.get("*", serveStaticFiles("./public/"));

	auto settings = new HTTPServerSettings;
	settings.port = to!ushort(environment.get("PORT", "8080"));

	listenHTTP(settings, router);

	runApplication();
}
