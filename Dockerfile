FROM dlang2/dmd-ubuntu

ENV PORT 8080
EXPOSE 8080

RUN apt-get update; apt-get install -y git
RUN git clone https://gitlab.com/Sliya/opp
RUN git clone https://gitlab.com/Sliya/playground-opp
WORKDIR opp
RUN dub build
WORKDIR ../playground-opp
RUN cp ../opp/examples/* public/examples
RUN dub build

ENTRYPOINT ["/playground-opp/playground-opp"]
