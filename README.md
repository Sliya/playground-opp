# Playground OPP

This is an online playground written in [D](https://dlang.org) to demonstrate the ability of my [programming language opp](https://gitlab.com/Sliya/opp)

## Run

To run it, you should download [opp](https://gitlab.com/Sliya/opp) in the parent directory and build it.
Then, simply run `dub` and it will start a server accessible at [http://localhost:8080](http://localhost:8080)
