document.getElementById('submit').onclick = run

CodeMirror.defineSimpleMode("opp", {
	// The start state contains the rules that are initially used
	start: [
		// The regex matches the token, the token property contains the type
		{regex: /"[^"]*"/, token: "string"},
		// You can match multiple tokens at once. Note that the captured
		// groups must span the whole string in this case
		{regex: /(fun)(\s+)([a-z$][\w$]*)/,
			token: ["keyword", null, "def"]},
		// Rules are matched in the order in which they appear, so there is
		// no ambiguity between this one and the one above
		{regex: /(?:fun|let|mut|return|break|if|while|else)\b/,
			token: "keyword"},
		{regex: /true|false|nil/, token: "atom"},
		{regex: /\d+(\.\d+)?/, token: "number"},
		{regex: /\/\/.*/, token: "comment"},
		{regex: /\/(?:[^\\]|\\.)*?\//, token: "variable-3"},
			// A next property will cause the mode to move to a different state
			{regex: /\/\*/, token: "comment", next: "comment"},
			{regex: /[-+\/*=<>!]+/, token: "operator"},
				// indent and dedent properties guide autoindentation
								  {regex: /[\{\[\(]/, indent: true},
									  {regex: /[\}\]\)]/, dedent: true},
										  {regex: /[a-z$][\w$]*/, token: null},
		],
				// The multi-line comment state.
				comment: [
					{regex: /.*?\*\//, token: "comment", next: "start"},
						{regex: /.*/, token: "comment"}
					],
				// The meta property contains global information about the mode. It
				// can contain properties like lineComment, which are supported by
				// all modes, and also directives like dontIndentStates, which are
				// specific to simple modes.
				meta: {
					dontIndentStates: ["comment"],
					lineComment: "//"
				}
					});


const cm = CodeMirror.fromTextArea(document.getElementById("source"), {
	lineNumbers: true,
	mode: "opp",
	theme: 'material',
	lineWrapping: true,
})
cm.setSize('100%', '98%')

const runBtn = document.getElementById("submit")
const res = document.getElementById('res')

function run(e) {
	runBtn.classList.toggle("is-loading")
	e.preventDefault()
	const form = new FormData()
	form.append('source', cm.getValue());
	fetch('/run', {method:'POST', body: form})
		.then(response => {
			response.text().then(text => {
				res.innerHTML = text.replaceAll('\n','<br/>')
				runBtn.classList.toggle("is-loading")
			})
		})
}
